package com.example.numbers.controller;


import com.example.numbers.service.NomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/number")
public class NomerController {

    private final NomerService service;

    @Autowired
    public NomerController(NomerService service) {
        this.service = service;
    }

    @GetMapping("/random")
    String getById(Model model) {
        String number = service.getRandom().toString();
        model.addAttribute("number", String.format("%s  %s", number.substring(0,6), number.substring(6)));
        return "index";
    }


    @GetMapping("/next")
    String getNext(Model model) {
        String number = service.getNext().toString();
        model.addAttribute("number", String.format("%s  %s", number.substring(0,6), number.substring(6)));
        return "index";
    }
}
