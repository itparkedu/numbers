package com.example.numbers.model;

import javax.persistence.*;

@Entity
@Table(name = "numbers")
public class Nomer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "auto_number")
        private String nomer;

    @Column(name = "enable")
    private Boolean enable ;

    public Nomer() {
    }

    public Nomer(String nomer, Boolean enable) {
        this.nomer = nomer;
        this.enable = enable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomer() {
        return nomer;
    }

    public void setNomer(String nomer) {
        this.nomer = nomer;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return nomer;
    }
}
