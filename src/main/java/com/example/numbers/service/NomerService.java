package com.example.numbers.service;

import com.example.numbers.exception.NumberOverException;
import com.example.numbers.model.Nomer;
import com.example.numbers.repository.NomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NomerService {

    private final NomerRepository repository;

    public NomerService(NomerRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public Nomer getRandom() {
        Nomer nomer = repository.random().orElseThrow(() -> new NumberOverException("method: random"));
        nomer.setEnable(true);
        repository.setLastNumber(nomer.getId());
        return repository.save(nomer);
    }

    @Transactional
    public Nomer getNext() {
        Nomer nomer = repository.next().orElseThrow(() -> new NumberOverException("method: next"));
        nomer.setEnable(true);
        repository.setLastNumber(nomer.getId());
        return repository.save(nomer);
    }

}
