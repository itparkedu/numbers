package com.example.numbers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice(annotations = RestController.class)
public class ExceptionInfoHandler {
    @ExceptionHandler(NumberOverException.class)
    protected ResponseEntity<Object> handleEntityNotFoundEx(NumberOverException e, WebRequest request) {
        ApiError apiError = new ApiError("Number is over Exception", e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }
}
