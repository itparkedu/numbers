package com.example.numbers.exception;

public class NumberOverException extends RuntimeException {
    public NumberOverException(String message) {
        super(message);
    }
}
