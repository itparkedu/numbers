package com.example.numbers.repository;

import com.example.numbers.model.Nomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface NomerRepository extends JpaRepository<Nomer, Integer> {

    @Query(value = "SELECT * FROM numbers n where n.enable = false ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
    Optional<Nomer> random();

    @Query(value = "SELECT * FROM numbers n WHERE n.enable = FALSE AND n.id > (SELECT val FROM lastused WHERE lastused.id=1) ORDER BY n.id LIMIT 1", nativeQuery = true)
    Optional<Nomer> next();

    @Modifying
    @Transactional
    @Query(value = "UPDATE lastused l SET val =:id where l.id = 1", nativeQuery = true)
    void setLastNumber(@Param("id") int id);

}
